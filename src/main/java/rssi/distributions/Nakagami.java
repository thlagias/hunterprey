package rssi.distributions;

import processing.core.PApplet;
import processing.data.Table;
import processing.data.TableRow;


public class Nakagami {
	private PApplet p;
	private int m; //Nakagami shape parameter
	private int index;
	private Table fadingTable;

	
	public Nakagami(int shapeParam) {
		p = new PApplet();
		m = shapeParam;
		index = 0;
		fadingTable = p.loadTable("input/multipath/Nakagami_PowerValues_dB.csv");
	}

	public float getRandomValue() {
		index++;
		TableRow row = fadingTable.getRow(index);
		float value  = row.getFloat(m-1);
		return value; //<>//
	}

	//////  Setters //////
	public void setShapeParam(int shapeParam) {
		m = shapeParam;
	}
}
