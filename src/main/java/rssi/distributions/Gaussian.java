package rssi.distributions;

import java.util.Random;


public class Gaussian {
	private Random random;
	private float mean;
	private float stdDev;

	public Gaussian(float meanValue, float standardDeviation) {
		random = new Random();
		mean = meanValue;
		stdDev = standardDeviation;
	}

	public float getRandomValue() {
		float value = (float)random.nextGaussian();
		value = value * stdDev;
		value = value + mean;
		return value;
	}
}
