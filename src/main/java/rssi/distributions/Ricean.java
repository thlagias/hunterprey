package rssi.distributions;

import processing.core.PApplet;
import processing.data.Table;
import processing.data.TableRow;


public class Ricean {
	private PApplet p;
	private int k; //Ricean shape parameter
	private int index;
	private Table fadingTable;

	
	public Ricean(int shapeParam) {
		p = new PApplet();
		k = shapeParam;
		index = 0;
		fadingTable = p.loadTable("input/multipath/Ricean_PowerValues_dB.csv");
	}

	public float getRandomValue() {
		index++;
		TableRow row = fadingTable.getRow(index);
		float value  = row.getFloat(k-1);
		return value;
	}

	//////  Setters //////
	public void setShapeParam(int shapeParam) {
		k = shapeParam;
	}
}
