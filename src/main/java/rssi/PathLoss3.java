package rssi;

import processing.core.PVector;
import rssi.distributions.Gaussian;
import rssi.distributions.Ricean;
import util.Util;

public class PathLoss3 implements PathLoss{
	private Gaussian gaussian;
	private Ricean   ricean;
	private float distanceReal;
	private float distanceObs;
	private float PL;    //Path Loss
	private float FSPL;  //Free Space Path Loss
	private float f_Hz;  //frequency (Hz)
	private float f_GHz; //frequency (GHz)
	private float h_BS;  //transmitter's antenna height
	private float h_MS;  //receiver's    antenna height
	private float Xg;    //slow fading (shadowing)
	private float Fg;    //fast fading (multipath)



	public PathLoss3() {
		gaussian = new Gaussian(0, 1.5f);
		ricean   = new Ricean(1);
		f_GHz = 2.4f;
		f_Hz = f_GHz * (float)Math.pow(10, 9);
		h_BS = 1;
		h_MS = 1;
	}

	public float getObservation(PVector transmiterPosition, PVector receiverPosition) {
		distanceReal = PVector.dist(transmiterPosition, receiverPosition)/10; //convert to m
		FSPL = getFreeSpacePathLoss(distanceReal);
		Xg   = getSlowFading();
		Fg   = getFastFading();
		PL   = FSPL + Xg + Fg;
		distanceObs = getInversePathLoss(PL);
		return distanceObs*10; //convert back to pixels
	}


	private float getFreeSpacePathLoss(float distance) {
		float g1 = 21.5f * Util.log10(distance) + 44.2f + 20*Util.log10(f_GHz/5.0f);
		float g2 = 10.5f - 18.5f*Util.log10(h_BS*h_MS) + 40*Util.log10(distance) + 1.5f*Util.log10(f_GHz/5.0f);
		return Math.max(g1, g2);
	}

	private float getSlowFading() {
		return gaussian.getRandomValue();
	}

	private float getFastFading() {
		return ricean.getRandomValue();
	}

	private float getInversePathLoss(float pathLoss) {
		float d1 = (float)Math.pow(10, (pathLoss - 44.2 - 20*Util.log10(f_GHz/5.0f)) / 21.5);
		float d2 = (float)Math.pow(10, (pathLoss - 10.5 + 18.5*Util.log10(h_BS*h_MS) - 1.5*Util.log10(f_GHz/5.0f)) / 40);
		return Math.min(d1, d2);
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  SETTERS - OF PATH LOSS
	//
	//////////////////////////////////////////////////////////////////////
	public void setFrequency(float freq) { //in GHz
		f_GHz = freq;
		f_Hz = freq * (float)Math.pow(10, 9);
	}

	public void setSlowFadingSD(float stdDev) {
		gaussian = new Gaussian(0, stdDev);
	}

	public void setFastFadingK(int k) {
		ricean.setShapeParam(k);
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  GETTERS
	//
	//////////////////////////////////////////////////////////////////////
	public float getDistanceReal() {
		return distanceReal;
	}

	public float getDistanceObs() {
		return distanceObs;
	}
}
