package rssi;

import processing.core.PVector;


public interface PathLoss {
	public float getObservation(PVector transmiterPosition, PVector receiverPosition);
	public float getDistanceReal();
	public float getDistanceObs();
}
