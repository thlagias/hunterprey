package rssi;

import processing.core.PVector;
import rssi.distributions.Gaussian;
import util.Util;


public class PathLoss1 implements PathLoss{
	private Gaussian gaussian;
	private float distanceReal;
	private float distanceObs;
	private float PL;    //Path Loss
	private float FSPL;  //Free Space Path Loss
	private float n;     //Path Loss exponent
	private float dn;    //Path Loss exponent deviation
	private float f_Hz;  //frequency (Hz)
	private float f_GHz; //frequency (GHz)
	private float Xg;    //slow fading (shadowing)
	private float Fg;    //fast fading (multipath)


	public PathLoss1() {
		gaussian = new Gaussian(0f, 1.5f);
		n     = 2.2f;
		dn    = 0;
		f_GHz = 2.4f;
		f_Hz  = f_GHz * (float)Math.pow(10, 9);
	}


	public float getObservation(PVector transmiterPosition, PVector receiverPosition) {
		distanceReal = PVector.dist(transmiterPosition, receiverPosition)/10; //convert to m
		FSPL = getFreeSpacePathLoss(distanceReal);
		Xg = getSlowFading();
		Fg = getFastFading();
		PL = FSPL + Xg + Fg; // Fg = 0 !!
		distanceObs = getInversePathLoss(PL);
		return distanceObs*10; //convert back to pixels
	}

	private float getFreeSpacePathLoss(float distance) {
		return 10*n*Util.log10(distance) + 20*Util.log10(f_Hz) - 147.55f;
	}

	private float getSlowFading() {
		return gaussian.getRandomValue();
	}

	private float getFastFading() {
		return 0;
	}

	private float getInversePathLoss(float pathLoss) {
		return (float)Math.pow(10, (pathLoss - 20*Util.log10(f_Hz) + 147.55) / (10*(n+dn)));
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  SETTERS - OF PATH LOSS
	//
	//////////////////////////////////////////////////////////////////////
	public void setFrequency(float freq) { //in GHz
		f_GHz = freq;
		f_Hz = freq * (float)Math.pow(10, 9);
	}

	public void setSlowFadingSD(float stdDev) {
		gaussian = new Gaussian(0, stdDev);
	}

	public void setPathLossExponent(float pathLossExponent) {
		n = pathLossExponent;
	}

	public void setPathLossExpDev(float deviation) {
		dn = deviation;
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  GETTERS
	//
	//////////////////////////////////////////////////////////////////////
	public float getDistanceReal() {
		return distanceReal;
	}

	public float getDistanceObs() {
		return distanceObs;
	}
}
