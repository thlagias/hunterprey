package hunter.tracking;

import java.util.ArrayList;
import animation.Episode;
import processing.core.PApplet;
import processing.core.PVector;
import util.Util;


public class Lct implements TrackingAlgorithm {
	private PApplet p;
	private Episode e;
	private float interval;
	private int counter;
	private ArrayList<Float> observations;
	private ArrayList<PVector> observationPositions;
	private ArrayList<PVector> circleDisplayPositions;//**
	private int index; //index of last observation
	private ArrayList<Float> approacingRatios;
	private float approachingRatioDelta;
	private boolean computeNewSteeringAngle;
	//** only for display purposes
	private PVector desired;
	private float steeringAngle;
	private float steeringAngleCoefficient; // percentage of the calculated steering angle
	private int turnSide;
	private boolean computeNewDesiredVelocity;
	private float noSpeed = 0.0001f;
	private boolean stopped = false;


	public Lct(PApplet parent, Episode episode, float period, float steeringCoefficient) {
		p = parent;
		e = episode;
		counter = 0;
		interval = period; //already in frames
		observations = new ArrayList<Float>();
		observationPositions = new ArrayList<PVector>();
		circleDisplayPositions = new ArrayList<PVector>();
		approacingRatios = new ArrayList<Float>();
		steeringAngleCoefficient = steeringCoefficient;
		turnSide = 1;
		desired = new PVector(p.random(2), p.random(2));
		desired.setMag(e.getHunter().getMaxSpeed());
	}



	/////////////////////////////////////////////////////////////////////
	//
	//  track() Part
	//
	//////////////////////////////////////////////////////////////////////
	public void run() {
		updateObservations();
		estimate();
		steer();
		display();
	}



	//////////////////////////////////////////////////////////////////////
	//
	//  updateObservations() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void updateObservations() {
		counter++;
		if (counter == interval) { // both counter and interval are in frames
			observations.add(getObservation());
			observationPositions.add(e.getHunter().getLocalPosition().copy());
			circleDisplayPositions.add(e.getHunter().getDisplayPosition().copy());
			index = observations.size()-1;
			counter = 0;
			computeNewSteeringAngle = true;
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  estimate() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void estimate() {
		findSteeringAngle();
		//decideTurn();
		varyInterval();
	}

	private void findSteeringAngle() {
		if (index > 0 && computeNewSteeringAngle) {
			computeNewSteeringAngle = false;
			float distTraveled = PVector.dist(observationPositions.get(index), observationPositions.get(index-1));
			float obs1 = observations.get(index-1);
			float obs2 = observations.get(index);
			float value = (p.pow(obs1, 2) - p.pow(obs2, 2) - p.pow(distTraveled, 2))/(-2 * (obs2*distTraveled));
			steeringAngle = 180 - p.degrees(p.acos(p.constrain(value, -1, 1)));
			approacingRatios.add((obs1-obs2)/distTraveled);
			int ind = approacingRatios.size();
			if (index>1) approachingRatioDelta = approacingRatios.get(ind-1) - approacingRatios.get(ind-2);
			computeNewDesiredVelocity = true;
			if (approacingRatios.get(ind-1)>0.8) computeNewDesiredVelocity = false;
		}
	}

	private void decideTurn() {
		if (computeNewDesiredVelocity) {
			if (approachingRatioDelta  < 0) {
				turnSide = -turnSide;
			}
		}
	}

	private void varyInterval() {
		if (index > 1 && computeNewDesiredVelocity) {
			float[] values = new float[2];
			values[0] = observations.get(index-1);
			values[1] = observations.get(index);
			if (Util.average(values) < 200) {
				interval = 20;
			} else if (Util.average(values) < 400) {
				interval = 40;
			} else if (Util.average(values) < 600) {
				interval = 60;
			} else {
				interval = 80;
			}

			//if (average(values) < 100) {
			//  interval = 10;
			//} else if (average(values) < 200) {
			//  interval = 20;
			//} else if (average(values) < 300) {
			//  interval = 30;
			//} else if (average(values) < 400) {
			//  interval = 40;
			//} else if (average(values) < 500) {
			//  interval = 50;
			//} else if (average(values) < 600) {
			//  interval = 60;
			//} else {
			//  interval = 80;
			//}
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  steer() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void steer() {
		if (computeNewDesiredVelocity && !stopped) { 
			// compute desired velocity
			desired = e.getHunter().getLocalVelocity().copy();
			desired.rotate(p.radians(turnSide * (steeringAngle * steeringAngleCoefficient)));
			desired.setMag(e.getHunter().getMaxSpeed());
			computeNewDesiredVelocity = false;
		}
		// check arrive
		stopMoving();
		startMoving();
		//compute steering force
		PVector steer = PVector.sub(desired, e.getHunter().getLocalVelocity());
		steer.limit(e.getHunter().getMaxForce());
		e.getHunter().applyForce(steer);
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  display() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void display() {
		displayCircles();
	}

	private void displayCircles() {
		if (index > 1) {
			for (int i=index-1; i<=index; i++) {
				PVector circleCenter = circleDisplayPositions.get(i);
				float circleDiameter = observations.get(i) * 2;
				p.fill(255, 0);
				p.stroke(0, 255, 255, 150);
				p.ellipse(circleCenter.x, circleCenter.y, circleDiameter, circleDiameter);
			}
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  start() & stop() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void startMoving() {
		boolean divergeTarget = observations.size()>0 && observations.get(index) > e.getHunter().getArrivalThreshold();
		if (divergeTarget) {
			desired.setMag(e.getHunter().getMaxSpeed());
			stopped = false;
		}
	}

	private void stopMoving() {
		boolean reachTarget = observations.size()>0 && observations.get(index) < e.getHunter().getArrivalThreshold();
		if (reachTarget) {
			desired.setMag(noSpeed);
			stopped = true;
		}
	}
	
	
	private float getObservation() {
		return e.getPathloss().getObservation(e.getPrey().getGlobalPosition(), e.getHunter().getGlobalPosition());
	}
}
