package hunter.tracking;

import java.util.ArrayList;
import Jama.Matrix;
import animation.Episode;
import processing.core.PApplet;
import processing.core.PVector;


public class Lls implements TrackingAlgorithm {
	private PApplet p;
	private Episode e;
	private boolean initiate = false;
	private float interval;
	private int counter;
	private ArrayList<Float>   observations;
	private ArrayList<PVector> observationPositions;
	private ArrayList<PVector> circleDisplayPositions;
	private int index; //index of last observation
	private int windowSize; // >2
	private int windowCounter;
	private double [] obsWindow;
	private PVector[] obsPositions;
	private PVector   approximateCoords;
	private double[]   Yo;
	private double[]   b;
	private double[][] A;
	boolean targetPositionExist = false;
	private PVector targetPosition;
	private PVector trilatPosition;
	private PVector desired;
	private float noSpeed = 0.0001f;
	private boolean stopped = false;



	public Lls(PApplet parent, Episode episode, float period, int windowLength) {
		p = parent;
		e = episode;
		interval = period;
		observations = new ArrayList<Float>();
		observationPositions = new ArrayList<PVector>();
		circleDisplayPositions = new ArrayList<PVector>();
		desired = new PVector(p.random(2), p.random(2));
		desired.setMag(e.getHunter().getMaxSpeed());
		windowSize = windowLength;
		obsWindow = new double[windowSize];
		obsPositions = new PVector[windowSize];
		targetPosition = new PVector(500, 350);
		trilatPosition = new PVector(500, 350);
	}



	/////////////////////////////////////////////////////////////////////
	//
	//  run() Part
	//
	//////////////////////////////////////////////////////////////////////
	public void run() {
		updateObservations();
		estimate();
		steer();
		display();
	}



	//////////////////////////////////////////////////////////////////////
	//
	//  updateObservations() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void updateObservations() {
		counter++;
		if (counter == interval) { // both counter and interval are in frames
			observations.add(getObservation());
			observationPositions.add(e.getHunter().getLocalPosition().copy());
			circleDisplayPositions.add(e.getHunter().getDisplayPosition().copy());
			index = observations.size()-1;
			windowCounter++;
			obsWindow[windowCounter-1] = observations.get(index);
			obsPositions[windowCounter-1] = observationPositions.get(index);
			counter = 0;
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  estimate() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void estimate() {
		if (windowSize == windowCounter) {
			windowCounter = 0;
			targetPositionExist = false;
			for (int i = 0; i < 5; i++) {
				runLeastSquaresEstimation();
			}
		}
	}

	private void runLeastSquaresEstimation() {
		Matrix Xo  = new Matrix(find_Xo(), 2);
		find_Yo();
		Matrix b   = new Matrix(find_b(), windowSize);
		Matrix A   = new Matrix(find_A());
		Matrix A_T = A.transpose();
		Matrix N   = A_T.times(A);
		Matrix u   = A_T.times(b);
		double det = N.det();
		if (det != 0 && !Double.isNaN(det)) {
			Matrix Xest = N.inverse().times(u);
			Matrix Xa = Xo.plus(Xest);
			targetPosition.set((float)Xa.get(0, 0), (float)Xa.get(1, 0));
			targetPositionExist = true;
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  steer() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void steer() {
		desired = PVector.sub(targetPosition, e.getHunter().getLocalPosition());
		// check arrive
		stopMoving();
		startMoving();
		PVector steer = PVector.sub(desired, e.getHunter().getLocalVelocity());
		steer.limit(e.getHunter().getMaxForce());
		e.getHunter().applyForce(steer);
	}



	//////////////////////////////////////////////////////////////////////
	//
	//  start() & stop() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void startMoving() {
		boolean divergeTarget = observations.size()>0 && observations.get(index) > e.getHunter().getArrivalThreshold();
		if (divergeTarget) {
			desired.setMag(e.getHunter().getMaxSpeed());
			stopped = false;
		}
	}

	private void stopMoving() {
		boolean reachTarget = observations.size()>0 && observations.get(index) < e.getHunter().getArrivalThreshold();
		if (reachTarget) {
			desired.setMag(noSpeed);
			stopped = true;
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  display() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void display() {
		displayCircles();
		displayTarget();
	}

	private void displayCircles() {
		if (index > windowSize) {
			for (int i=circleDisplayPositions.size()-windowSize; i<=index; i++) {
				PVector circleCenter = circleDisplayPositions.get(i);
				float circleDiameter = observations.get(i) * 2;
				p.fill(255, 0);
				p.stroke(0, 255, 255, 150);
				p.ellipse(circleCenter.x, circleCenter.y, circleDiameter, circleDiameter);
			}
		}
	}

	private void displayTarget() {
		p.stroke(0);
		p.fill(100);
		p.ellipse(targetPosition.x + (e.getHunter().getGlobalPosition().x-e.getHunter().getLocalPosition().x), 
				-(targetPosition.y + (e.getHunter().getGlobalPosition().y-e.getHunter().getLocalPosition().y)), 22, 22);
		p.stroke(0);
		p.fill(255, 0, 0);
		p.ellipse(trilatPosition.x + (e.getHunter().getGlobalPosition().x-e.getHunter().getLocalPosition().x), 
				-(trilatPosition.y + (e.getHunter().getGlobalPosition().y-e.getHunter().getLocalPosition().y)), 22, 22);
	}



	//////////////////////////////////////////////////////////////////////
	//
	//  utils Part
	//
	//////////////////////////////////////////////////////////////////////
	private double[] find_Xo() {
		if (targetPositionExist) {
			approximateCoords = targetPosition;
		} else {
			double [] obs    = new double [3];
			PVector[] obsPos = new PVector[3];
			obs[0]    = observations.get(index-2);
			obs[1]    = observations.get(index-1);
			obs[2]    = observations.get(index-0);
			obsPos[0] = observationPositions.get(index-2);
			obsPos[1] = observationPositions.get(index-1);
			obsPos[2] = observationPositions.get(index-0);
			approximateCoords = trilateration(obsPos, obs);
		}
		return new double[]{approximateCoords.x, approximateCoords.y};
	}

	private void find_Yo() {
		Yo = new double[windowSize];
		for (int i=0; i < windowSize; i++) {
			Yo[i] = PVector.dist(approximateCoords, obsPositions[i]);
		}
	}

	private double[] find_b() {
		double[] result = new double[windowSize]; 
		for (int i=0; i<windowSize; i++) {
			result[i] = obsWindow[i] - Yo[i];
		}
		return result;
	}

	private double[][] find_A() {
		double[][] alpha = new double[windowSize][2];
		for (int i=0; i<windowSize; i++) {
			for (int j=0; j<2; j++) {
				if (j==0) {
					alpha[i][j] = (approximateCoords.x-obsPositions[i].x)/Yo[i];
				} else {
					alpha[i][j] = (approximateCoords.y-obsPositions[i].y)/Yo[i];
				}
			}
		}
		return alpha;
	}


	private PVector trilateration(PVector[] referencePoints, double[] observations) {

		PVector point1 = referencePoints[0];
		PVector point2 = referencePoints[1];
		PVector point3 = referencePoints[2];
		double obs1 = observations[0];
		double obs2 = observations[1];
		double obs3 = observations[2];

		float A = p.pow(point1.x, 2) + p.pow(point1.y, 2) - p.pow((float)obs1, 2);
		float B = p.pow(point2.x, 2) + p.pow(point2.y, 2) - p.pow((float)obs2, 2);
		float C = p.pow(point3.x, 2) + p.pow(point3.y, 2) - p.pow((float)obs3, 2);

		float X32 = point3.x - point2.x;
		float X13 = point1.x - point3.x;
		float X21 = point2.x - point1.x;
		float Y32 = point3.y - point2.y;
		float Y13 = point1.y - point3.y;
		float Y21 = point2.y - point1.y;

		float X = ((A*Y32)+(B*Y13)+(C*Y21)) / (2*((point1.x*Y32)+(point2.x*Y13)+(point3.x*Y21)));
		float Y = ((A*X32)+(B*X13)+(C*X21)) / (2*((point1.y*X32)+(point2.y*X13)+(point3.y*X21)));
		trilatPosition = new PVector(X, Y);

		if (Double.isInfinite(trilatPosition.x)  || Double.isInfinite(trilatPosition.y) || 
				Double.isNaN(trilatPosition.x)       || Double.isNaN(trilatPosition.y))  {
			return new PVector(0, 0);
		} else {  
			return trilatPosition;
		}
	}
	
	private float getObservation() {
		return e.getPathloss().getObservation(e.getPrey().getGlobalPosition(), e.getHunter().getGlobalPosition());
	}
}
