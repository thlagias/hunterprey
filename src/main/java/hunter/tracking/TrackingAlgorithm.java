package hunter.tracking;

import hunter.Hunter;


public interface TrackingAlgorithm {
	public void run();
}
