package hunter.tracking;

import processing.core.PApplet;
import processing.core.PVector;


public class Particle implements Comparable {
	private PApplet p;
	private PVector localPosition;
	private PVector localVelocity;
	private float   weight = 1;
	private float   weightNorm;
	//global
	private PVector globalPosition;
	//display
	private PVector displayPosition;


	public Particle(PApplet parent, PVector position, PVector velocity) {
		p = parent;
		localPosition = position;
		localVelocity = velocity;
		globalPosition = new PVector();
		displayPosition = new PVector();
	}

	public Particle(PApplet parent, PVector observationPoint, float observation, float preySpeed) {
		p = parent;
		float theta = p.random(0, p.TWO_PI);
		float r = observation;
		PVector particleRelativeLocation = new PVector(r*p.sin(theta), r*p.cos(theta));
		localPosition = PVector.add(observationPoint, particleRelativeLocation);
		PVector particleVelocity = new PVector(1, 1);
		particleVelocity.setMag(p.random(0, preySpeed)); // Prey maxSpeed = 1
		particleVelocity.rotate(p.random(0, p.TWO_PI));
		localVelocity = particleVelocity;
		globalPosition = new PVector();
		displayPosition = new PVector();
	}


	public Particle copy() {
		Particle par = new Particle(this.p, this.localPosition.copy(), this.localVelocity.copy());
		par.weight = this.weight;
		par.weightNorm = this.weightNorm;
		par.globalPosition = this.globalPosition.copy();
		par.displayPosition = this.displayPosition.copy();
		return par;
	}


	public void update(float interval, float noise) {
		//add noise to particles velocity
		localVelocity.rotate(noise);
		localVelocity.setMag(localVelocity.mag() + noise);
		localPosition.add(PVector.mult(localVelocity, interval));
		//localPosition.add(localVelocity);
	}

	public void calculateWeight(PVector observationPoint, float observation, float sd) {
		float m = observation; //sensor range Rt
		float x = PVector.dist(observationPoint, localPosition); //range to particle Rp
		float sigma = sd; //standard deviation of particles ranges(Rp's) with last observation(Rt) as a mean value
		weight *= ( (p.exp((-p.pow(x-m, 2))/(2*p.pow(sigma, 2)))) / (sigma*(p.sqrt(p.TWO_PI)))  );
	}

	public void normalizeWeight(float weightSum) {
		weightNorm = weight/weightSum;
		weight = weightNorm;
		// we use the weightNorm as the weight, 
		// so that on the next iteration we will use 
		// the weightNorm to calculateWeight();
	}

	public void display(float maxWeightNorm, PVector globalPosition, PVector localPosition) {
		// covert local coords to global coords
		globalPosition.set(localPosition.x + (globalPosition.x-localPosition.x), 
				-(localPosition.y + (globalPosition.y-localPosition.y)));
		displayPosition.set(globalPosition.x, globalPosition.y);
		float colorValue = p.map(weightNorm, 0, maxWeightNorm, 0, 255);
		p.stroke(colorValue, 0, 150-colorValue);
		p.fill(colorValue, 0, 150-colorValue);
		p.ellipse(displayPosition.x, displayPosition.y, 1, 1);
	}


	public int compareTo(Object o) {
		Particle par = (Particle)o;
		if (this.weightNorm <= par.weightNorm) return -1;
		else if (this.weightNorm > par.weightNorm) return 1;
		return 0;
	}


	///////////////////////////////////////////////////////////
	//  Getters
	///////////////////////////////////////////////////////////
	public float getWeight() {
		return weight;
	}

	public float getWeightNorm() {
		return weightNorm;
	}

	public PVector getLocalPosition() {
		return localPosition;
	}

	public PVector getDisplayPosition() {
		return displayPosition;
	}
}
