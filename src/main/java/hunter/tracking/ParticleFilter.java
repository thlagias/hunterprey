package hunter.tracking;

import java.util.ArrayList;
import java.util.Collections;
import animation.Episode;
import processing.core.PApplet;
import processing.core.PVector;
import rssi.distributions.Gaussian;


public class ParticleFilter implements TrackingAlgorithm {
	private PApplet p;
	private Episode e;
	private float interval;
	private int counter;
	private boolean proceed = false;
	private boolean initiate = false;
	private ArrayList<Float> observations;
	private ArrayList<PVector> observationPositions;
	private ArrayList<PVector> circleDisplayPositions;//**
	private int index; //index of last observation
	private float effectiveN; //see algorithm
	private float thresholdN; //see algorithm
	//** only for display purposes
	// PARTICLES
	private ArrayList<Particle> particles;
	private int n; //number of particles
	private float rangeSD; //standard deviation of particles from range observation(as a mean)
	private float sumWeight;           //sum of particles' weights
	private float sumWeightNorm;       //sum of particles' normalized weights
	private float sumWeightNormSquare; //sum of particles' square normalized weights
	private float preySpeed;           //approximate value of Prey's maximum speed
	private Gaussian noise; //noise for updating particles velocities and then positions
	private PVector desired;
	private float noSpeed = 0.0001f;
	private boolean stopped = false;



	public ParticleFilter(PApplet parent, Episode episode, float period, int particlesNumber, float sd, float preySpd) {
		p = parent;
		e = episode;
		counter = 0;
		interval = period; //already in frames
		observations = new ArrayList<Float>();
		observationPositions = new ArrayList<PVector>();
		circleDisplayPositions = new ArrayList<PVector>();
		particles = new ArrayList<Particle>();
		n = particlesNumber;
		noise = new Gaussian(0, sd);
		preySpeed = preySpd;
		desired = new PVector(p.random(2), p.random(2));
		desired.setMag(e.getHunter().getMaxSpeed());
	}



	/////////////////////////////////////////////////////////////////////
	//
	//  run() Part
	//
	//////////////////////////////////////////////////////////////////////
	public void run() {
		updateObservations();
		estimate();
		steer(); // compute steering force
		display();
	}



	//////////////////////////////////////////////////////////////////////
	//
	//  updateObservations() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void updateObservations() {
		counter++;
		if (counter == interval) { // both counter and interval are in frames
			observations.add(getObservation());
			observationPositions.add(e.getHunter().getLocalPosition().copy());
			circleDisplayPositions.add(e.getHunter().getDisplayPosition().copy());
			index = observations.size()-1;
			counter = 0;
			proceed = true;
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  estimate() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void estimate() {
		initialization();    // step 1
		prediction();        // step 2
		weightCalculation(); // step 3
		resampling();        // step 4
	}

	private void initialization() {
		if (proceed == true && initiate == false) {

			particles.clear();
			for (int i=0; i < n; i++) {
				//we calculate particle's position & velocity within the constructor
				Particle par = new Particle(p, observationPositions.get(index), observations.get(index), preySpeed);
				particles.add(par);
			}//end for

			initiate = true; //initialization runs only during the first cycle
			proceed = false; //during the first cycle only initialization runs
			//println("initialization");
		}//end if
	}

	private void prediction() {
		if (proceed == true) {
			for (Particle par : particles) {
				par.update(interval, noise.getRandomValue());
			}
		}
	}


	private void weightCalculation() {
		if (proceed == true) {

			updateRangeSD();
			for (Particle par : particles) {
				par.calculateWeight(observationPositions.get(index), observations.get(index), rangeSD);
			}

			updateSumWeight();
			for (Particle par : particles) {
				par.normalizeWeight(sumWeight);
			}

			Collections.sort(particles);
		}//end if
	}


	private void resampling() {
		if (proceed == true) {

			updateSumWeightNorm();
			effectiveN = 1/sumWeightNormSquare;
			thresholdN = n/2;
			//println(effectiveN);

			if (effectiveN < thresholdN) {

				//println("resampling");
				// http://robotics.stackexchange.com/questions/479/particle-filters-how-to-do-resampling 
				//1. Normalize weights.
				//2. Calculate an array of the cumulative sum of the weights.
				//3. Randomly generate a number & determine which range in that cumulative weight array to which the number belongs.
				//4. The index of that range would correspond to the particle that should be created.
				//5. Repeat until you have the desired number of samples.

				float sum = 0;
				ArrayList<Float> cumulativeSumOfWeights = new ArrayList<Float>();
				ArrayList<Integer> indexes = new ArrayList<Integer>();
				ArrayList<Particle> newParticles = new ArrayList<Particle>();

				for (int i = 0; i < particles.size(); i++) {
					//println(particles.get(i).weightNorm);
					if (particles.get(i).getWeightNorm() != 0) {
						sum = sum + particles.get(i).getWeightNorm();
						cumulativeSumOfWeights.add(sum);
						indexes.add(i);
					}
				}

				for (int i = 0; i < n; i++) {
					float r = p.random(0, sumWeightNorm);
					for (int j=0; j < cumulativeSumOfWeights.size(); j++) {
						if (r <= cumulativeSumOfWeights.get(j)) {
							//genarate a same particle with particle with index j: particles.get(j).copy()
							Particle par = particles.get(indexes.get(j)).copy();
							//set its weight 1/n (TEST)
							//par.weight = 1;
							//par.weightNorm = (float)1/n;
							//and add to a new ArrayList
							newParticles.add(par);
							break;
						}
					}
				}
				particles = new ArrayList<Particle>(newParticles);
				//println("size: " + particles.size());
			}//end if
		}
		proceed = false;
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  steer() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void steer() {
		if (sumWeightNorm != 0) {

			if (!stopped) {
				//compute desired velocity
				desired = computeDesiredHighest();
				//desired = computeDesiredMean();
			}
			// check arrive
			stopMoving();
			startMoving();
			//compute steering force
			PVector steer = PVector.sub(desired, e.getHunter().getLocalVelocity());
			steer.limit(e.getHunter().getMaxForce());
			e.getHunter().applyForce(steer);
		}
	}

	private PVector computeDesiredHighest() {
		Particle track = particles.get(n-1);
		//make this particle blue
		p.fill(255, 0, 0);
		p.ellipse(track.getDisplayPosition().x, track.getDisplayPosition().y, 10, 10);
		return PVector.sub(track.getLocalPosition(), e.getHunter().getLocalPosition());
	}

	private PVector computeDesiredMean() {
		float sumx = 0;
		float sumy = 0;
		for (int i = n-50; i < n; i++) {
			sumx += particles.get(i).getLocalPosition().x;
			sumy += particles.get(i).getLocalPosition().y;
		}
		PVector track = new PVector(sumx/50, sumy/50);
		p.fill(255, 200, 0);
		p.ellipse(track.x + (e.getHunter().getGlobalPosition().x-e.getHunter().getLocalPosition().x), 
				-(track.y + (e.getHunter().getGlobalPosition().y-e.getHunter().getLocalPosition().y)), 10, 10);
		return PVector.sub(track, e.getHunter().getLocalPosition());
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  display() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void display() {
		displayCircles();
		displayParticles();
	}

	private void displayCircles() {
		if (index > 1) {
			PVector circleCenter = circleDisplayPositions.get(index);
			float circleDiameter = observations.get(index) * 2;
			p.fill(255, 0);
			p.stroke(0, 255, 255, 150);
			p.ellipse(circleCenter.x, circleCenter.y, circleDiameter, circleDiameter);
		}
	}

	private void displayParticles() {
		for (Particle par : particles) {
			par.display(particles.get(n-1).getWeightNorm(), 
					e.getHunter().getGlobalPosition(), 
					e.getHunter().getLocalPosition());
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  start() & stop() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void startMoving() {
		boolean divergeTarget = observations.size()>0 && observations.get(index) > e.getHunter().getArrivalThreshold();
		if (divergeTarget) {
			desired.setMag(e.getHunter().getMaxSpeed());
			stopped = false;
		}
	}

	private void stopMoving() {
		boolean reachTarget = observations.size()>0 && observations.get(index) < e.getHunter().getArrivalThreshold();
		if (reachTarget) {
			desired.setMag(noSpeed);
			stopped = true;
		}
	}


	///////////////////////////////////////////////////////////
	//  Attributes of Particles set
	///////////////////////////////////////////////////////////

	//standard deviation of particles ranges(Rp's), with last observation(Rt) as a mean value
	private void updateRangeSD() {
		float sum = 0;
		for (Particle par : particles) {
			float range = PVector.dist(observationPositions.get(index), par.getLocalPosition());
			sum = sum + p.pow(range-(observations.get(index)), 2);
		}
		rangeSD = p.sqrt(sum/n);
	}

	//sum of the current weights of all particles
	private void updateSumWeight() {
		float sum = 0;
		for (Particle par : particles) {
			sum = sum + par.getWeight();
		}
		sumWeight = sum;
		if (sumWeight==0) {
			proceed = false;
			initiate = false;
		}
	}

	//sum of 
	// 1)normalized weights of all particles, 
	// 2)square normalized weights of all particles
	private void updateSumWeightNorm() {
		float sum1 = 0;
		float sum2 = 0;
		for (Particle par : particles) {
			sum1 = sum1 + par.getWeightNorm();
			sum2 = sum2 + p.pow(par.getWeightNorm(), 2);
		}
		sumWeightNorm = sum1;
		sumWeightNormSquare = sum2;
	}
	
	private float getObservation() {
		return e.getPathloss().getObservation(e.getPrey().getGlobalPosition(), e.getHunter().getGlobalPosition());
	}

	///////////////////////////////////////////////////////////
	//  Getters
	///////////////////////////////////////////////////////////

	//	ArrayList<Particle> getParticles() {
	//		return particles;
	//	}
	//
	//	ArrayList<Float> getObservationsList() {
	//		return observations;
	//	}
	//
	//	ArrayList<PVector> getObservationPositions() {
	//		return observationPositions;
	//	}
	//
	//	ArrayList<PVector> getCircleDisplayPositions() {
	//		return circleDisplayPositions;
	//	}
	//
	//	int getIndex() {
	//		return index;
	//	}
}
