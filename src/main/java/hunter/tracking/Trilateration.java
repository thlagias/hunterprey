package hunter.tracking;

import java.util.ArrayList;
import animation.Episode;
import processing.core.PApplet;
import processing.core.PVector;


public class Trilateration implements TrackingAlgorithm {
	private PApplet p;
	private Episode e;
	private int observationCount = 0; // can only be 1,2,3
	private PVector firstObservationMove;
	private PVector observationPosition;
	private boolean computeObservationPositions = true;
	private ArrayList<Float> observations;
	private ArrayList<PVector> observationPositions;
	private ArrayList<PVector> circleDisplayPositions;
	private int index;
	private boolean computeTargetPosition;
	private PVector targetPosition;
	private PVector desired;
	private boolean slidingWindow;
	private float noSpeed = 0.0001f;
	private boolean changeCirclesColor;


	public Trilateration(PApplet parent, Episode episode, boolean sliding) {
		p = parent;
		e = episode;
		slidingWindow  = sliding;
		observations = new ArrayList<Float>();
		observationPositions = new ArrayList<PVector>();
		circleDisplayPositions = new ArrayList<PVector>();
		targetPosition = new PVector(p.random(100), p.random(100));
	}



	/////////////////////////////////////////////////////////////////////
	//
	//  run() Part
	//
	//////////////////////////////////////////////////////////////////////
	public void run() {
		updateObservations();
		estimate();
		display();
	}



	//////////////////////////////////////////////////////////////////////
	//
	//  updateObservations() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void updateObservations() {
		computeNextObsPosition();
		getNextObservation(); // contains steer()
	}

	private void computeNextObsPosition() {
		if (computeObservationPositions) {
			observationCount++;

			if (observationCount == 1) {
				firstObservationMove = PVector.sub(targetPosition, e.getHunter().getLocalPosition());
				firstObservationMove.mult(0.6f);
				observationPosition = PVector.add(e.getHunter().getLocalPosition(), firstObservationMove);
			}

			if (observationCount == 2) {
				PVector secondObservationMove;
				if (!slidingWindow) {
					secondObservationMove = firstObservationMove.copy();
					secondObservationMove.rotate(p.radians(-45));
					secondObservationMove.setMag(80);
				} else { // slidingWindow
					secondObservationMove = PVector.sub(targetPosition, e.getHunter().getLocalPosition());
					secondObservationMove.rotate(p.radians(-45));
					secondObservationMove.setMag(80);
				}
				observationPosition = PVector.add(e.getHunter().getLocalPosition(), secondObservationMove);
			}

			if (observationCount == 3) {
				PVector thirdObservationMove;
				if (!slidingWindow) {
					thirdObservationMove = firstObservationMove.copy();
					thirdObservationMove.rotate(p.radians(90));
					thirdObservationMove.setMag(114); // Pythagorean
				} else { // slidingWindow
					thirdObservationMove = PVector.sub(targetPosition, e.getHunter().getLocalPosition());
					thirdObservationMove.rotate(p.radians(45));
					thirdObservationMove.setMag(80);
				}
				observationPosition = PVector.add(e.getHunter().getLocalPosition(), thirdObservationMove);
			}

			computeObservationPositions = false;
		}
	}

	private void getNextObservation() {
		steer(); // steer() to move to the next Observation Point
		boolean arriveAtObservationPosition = PVector.dist(e.getHunter().getLocalPosition(), observationPosition)<1;
		if (arriveAtObservationPosition) {
			observations.add(getObservation());
			observationPositions.add(e.getHunter().getLocalPosition().copy());
			circleDisplayPositions.add(e.getHunter().getDisplayPosition().copy());
			index = observations.size()-1;
			//not in every frame but only after getting an observation, we decide computeTargetPosition
			computeTargetPosition = (slidingWindow || observationCount==3) && observations.size()>=3;
			//we have the observation, so in any case(either computeTargetPosition or not)
			//we can move to the next observationPoint
			computeObservationPositions= true;
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  estimate() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void estimate() {
		computeTargetPosition();
	}

	private void computeTargetPosition() {
		if (computeTargetPosition) {
			computeTargetPosition = false;
			PVector point1 = observationPositions.get(index-2);
			PVector point2 = observationPositions.get(index-1);
			PVector point3 = observationPositions.get(index);
			float obs1 = observations.get(index-2);
			float obs2 = observations.get(index-1);
			float obs3 = observations.get(index);

			float A = p.pow(point1.x, 2) + p.pow(point1.y, 2) - p.pow(obs1, 2);
			float B = p.pow(point2.x, 2) + p.pow(point2.y, 2) - p.pow(obs2, 2);
			float C = p.pow(point3.x, 2) + p.pow(point3.y, 2) - p.pow(obs3, 2);

			float X32 = point3.x - point2.x;
			float X13 = point1.x - point3.x;
			float X21 = point2.x - point1.x;
			float Y32 = point3.y - point2.y;
			float Y13 = point1.y - point3.y;
			float Y21 = point2.y - point1.y;

			float X = ((A*Y32)+(B*Y13)+(C*Y21)) / (2*((point1.x*Y32)+(point2.x*Y13)+(point3.x*Y21)));
			float Y = ((A*X32)+(B*X13)+(C*X21)) / (2*((point1.y*X32)+(point2.y*X13)+(point3.y*X21)));
			targetPosition.set(X, Y);

			if (observationCount == 3) observationCount = 0;
		}
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  steer() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void steer() {
		desired = PVector.sub(observationPosition, e.getHunter().getLocalPosition());
		float d = desired.mag();
		desired.normalize();

		if (d >= 50) {
			// Move to observationPoint
			desired.mult(e.getHunter().getMaxSpeed());
			// Check Arrival at Target
			stopMoving();
			startMoving();
		} else {
			// Check Arrival at observationPoint
			float m = p.map(d, 0, 50, 0, e.getHunter().getMaxSpeed());
			desired.mult(m);
		}

		PVector steer = PVector.sub(desired, e.getHunter().getLocalVelocity());
		steer.limit(e.getHunter().getMaxForce());
		e.getHunter().applyForce(steer);
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  display() Part
	//
	//////////////////////////////////////////////////////////////////////
	private void display() {
		displayCirlces();
		displayTarget();
	}

	private void displayCirlces() {
		if (index > 1) {
			p.fill(255, 0);
			p.stroke(0, 255, 255, 150);
			int numberOfCircles=0;
			if (observationCount==2) numberOfCircles = 0;
			if (observationCount==3) numberOfCircles = 1;
			if (observationCount==0) numberOfCircles = 2;
			if (observationCount==1 || slidingWindow) numberOfCircles = 2;
			if (observationCount==0) changeCirclesColor=true;
			if (observationCount==2) changeCirclesColor=false;
			if (changeCirclesColor || slidingWindow) p.stroke(255, 0, 255);
			for (int i=index-numberOfCircles; i<=index; i++) {
				PVector circleCenter = circleDisplayPositions.get(i);
				float circleDiameter = observations.get(i) * 2;
				p.ellipse(circleCenter.x, circleCenter.y, circleDiameter, circleDiameter);
			}
		}
	}

	private void displayTarget() {
		p.stroke(0);
		p.fill(100);
		p.ellipse(targetPosition.x + (e.getHunter().getGlobalPosition().x-e.getHunter().getLocalPosition().x), 
				    -(targetPosition.y + (e.getHunter().getGlobalPosition().y-e.getHunter().getLocalPosition().y)), 22, 22);
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  start() & stop() Part
	//
	//////////////////////////////////////////////////////////////////////

	// Within the startMoving() method, 
	// - we use getObservation() instead of the last observation of the observations list,
	//   because when we are already stopped, we are not able to move to the next observationPoint 
	//   and gather observations. 
	// - These observations are collected only to enable us to know when to start moving again.
	//   So there is no need to add them into the observations list, or track the current observationPoint.

	private void startMoving() {
		boolean divergeTarget = observations.size()>0 && getObservation() > e.getHunter().getArrivalThreshold();
		if (divergeTarget) {
			desired.setMag(e.getHunter().getMaxSpeed());
		}
	}

	private void stopMoving() {
		boolean reachTarget = observations.size()>0 && observations.get(index) < e.getHunter().getArrivalThreshold();
		if (reachTarget) {
			desired.setMag(noSpeed);
		}
	}
	
	private float getObservation() {
		return e.getPathloss().getObservation(e.getPrey().getGlobalPosition(), e.getHunter().getGlobalPosition());
	}
}
