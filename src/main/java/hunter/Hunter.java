package hunter;

import java.util.ArrayList;
import hunter.tracking.TrackingAlgorithm;
import processing.core.PApplet;
import processing.core.PVector;
import util.Util;


public class Hunter {
	private PApplet p;
	//algorithms
	private TrackingAlgorithm trackingAlgorithm;
	//local
	private PVector localPosition;
	private PVector localVelocity;
	private PVector localAcceleration;
	private PVector totalForce;
	private PVector localSteering;
	private int     localSteeringAngle;
	private PVector localTargetPosition;
	private PVector localTargetVelocity;
	//global
	private PVector initialize;
	private PVector globalPosition;
	private PVector globalVelocity;
	private PVector globalAcceleration;
	private float   maxSpeed;
	private float   maxforce;  
	private float   mass;
	private float   arrivalThreshold;
	//display
	private ArrayList<PVector> trajectory;
	private PVector displayPosition;
	private PVector displayVelocity;
	private float   hunterScale;
	private float   trajectorySize;
	private boolean displayTrajectory;


	public Hunter(PApplet parent, TrackingAlgorithm algorithm) {
		p = parent;
		// algorithm
		trackingAlgorithm = algorithm;
		// local
		localPosition = new PVector(0, 0);
		localVelocity = new PVector(0, 0);
		localAcceleration = new PVector(0, 0);
		// global
		//globalPosition = new PVector(200, 200);
		globalPosition = Util.getRandomPosition(p);
		initialize = globalPosition.copy();
		globalVelocity = new PVector(0, 0);
		globalAcceleration = new PVector(0, 0);
		totalForce = new PVector(0, 0);
		maxSpeed = 4; //m/s or ppf (no conversion needed)
		maxforce = 0.5f;
		mass = 1;
		arrivalThreshold = 60; //in pixels
		// display
		displayPosition = new PVector(globalPosition.x, -globalPosition.y);
		displayVelocity = new PVector(0, 0);
		trajectory      = new ArrayList<PVector>();
		hunterScale = 0.5f;
	}


	public void run() {
		trackingAlgorithm.run();
		localUpdate();
		globalUpdate();
		displayUpdate();
	}
	


	public void applyForce(PVector force) {
		totalForce.add(force);
	}

	private void localUpdate() {
		localAcceleration = totalForce.div(mass);
		localVelocity.add(localAcceleration);
		localVelocity.limit(maxSpeed);
		localPosition.add(localVelocity);
	}

	private void globalUpdate() {
		globalAcceleration = localAcceleration; 
		globalVelocity.add(globalAcceleration);
		globalVelocity.limit(maxSpeed);
		globalPosition.add(globalVelocity);       
		totalForce.mult(0);
	}


	private void displayUpdate() {
		displayPosition.set(globalPosition.x, -globalPosition.y);
		displayVelocity.set(globalVelocity.x, -globalVelocity.y);
		p.rectMode(p.CENTER);
		p.stroke(0, 0, 100);
		p.strokeWeight(1.1f);
		p.pushMatrix();
		p.translate(displayPosition.x, displayPosition.y);
		p.rotate(displayVelocity.heading());
		p.fill(200, 0, 255);
		p.beginShape();
		p.vertex(-16*hunterScale, -8*hunterScale);
		p.vertex(-8*hunterScale, -16*hunterScale);
		p.vertex(8*hunterScale, -16*hunterScale);
		p.vertex(16*hunterScale, -8*hunterScale);
		p.vertex(16*hunterScale, 8*hunterScale);
		p.vertex(8*hunterScale, 16*hunterScale);
		p.vertex(-8*hunterScale, 16*hunterScale);
		p.vertex(-16*hunterScale, 8*hunterScale);
		p.endShape(p.CLOSE);
		p.fill(200, 0, 255);
		p.rect(2*hunterScale, -20*hunterScale, 27*hunterScale, 8*hunterScale);
		p.rect(2*hunterScale, 20*hunterScale, 27*hunterScale, 8*hunterScale);
		p.popMatrix(); 

		//diplay trajectory
		if (displayTrajectory) {
			trajectory.add(displayPosition.copy());
			if (trajectory.size() > trajectorySize) {
				trajectory.remove(0);
			}
			p.beginShape();
			p.stroke(200, 0, 255);
			p.strokeWeight(1);
			p.noFill();
			for (PVector v : trajectory) {
				p.vertex(v.x, v.y);
			}
			p.endShape();
		}
	}




	//////////////////////////////////////////////////////////////////////
	//
	//  SETTERS
	//
	//////////////////////////////////////////////////////////////////////

	public void setMaxSpeed(float v) {
		//no need to convert
		maxSpeed = v;
	}

	public void setMaxForce(float f) {
		maxforce = f;
	}

	public void setMass(float m) {
		mass = m;
	}

	public void setArrivalThreshold(float d) {
		//convert m to pixels
		arrivalThreshold = d * 10;
	}

	public void setTrajectorySize(float size) {
		trajectorySize = size;
	}
	
	public void setTrajectoryDisplay(boolean display) {
		displayTrajectory = display;
	}
	
	
	//////////////////////////////////////////////////////////////////////
	//
	//  GETTERS
	//
	//////////////////////////////////////////////////////////////////////

	public float getMaxSpeed() {
		//no need to convert
		return maxSpeed;
	}

	public float getMaxForce() {
		return maxforce;
	}

	public float setMass() {
		return mass;
	}

	public float getArrivalThreshold() {
		//convert m to pixels
		return arrivalThreshold;
	}
	
	public PVector getLocalPosition() {
		return localPosition;
	}
	
	public PVector getLocalVelocity() {
		return localVelocity;
	}
	
	public PVector getGlobalPosition() {
		return globalPosition;
	}
	
	public PVector getGlobalVelocity() {
		return globalVelocity;
	}
	
	public PVector getDisplayPosition() {
		return displayPosition;
	}
	
	public PVector getDisplayVelocity() {
		return displayVelocity;
	}
}
