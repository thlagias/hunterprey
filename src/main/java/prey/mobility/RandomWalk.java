package prey.mobility;

import java.util.ArrayList;
import animation.Episode;
import processing.core.PApplet;
import processing.core.PVector;
import util.Util;


public class RandomWalk implements MobilityAlgorithm {
	private PApplet p;
	private Episode e;
	private float nextPositionRange;
	private PVector nextPosition;
	private ArrayList<PVector> randomWalkPositions;


	public RandomWalk(PApplet parent, Episode episode, float range) {
		p = parent;
		e = episode;
		//nextPositionRange = 400;
		setRandomWalkRange(range);
		randomWalkPositions = new ArrayList<PVector>();
		randomWalkPositions.add(e.getPrey().getGlobalPosition().copy());
		// In order to generate directly a new nextPositon
		// within the algorithms we set nextPosition = globalPosition
		nextPosition = e.getPrey().getGlobalPosition().copy();
	}



	public void run() {
		if (PVector.dist(nextPosition, e.getPrey().getGlobalPosition()) < 10) {
			randomWalkPositions.remove(0);
			nextPosition = randomWalkPositions.get(0);
		}
		PVector desired = PVector.sub(nextPosition, e.getPrey().getGlobalPosition());
		desired.normalize();
		desired.mult(e.getPrey().getMaxSpeed());
		PVector steer = PVector.sub(desired, e.getPrey().getGlobalVelocity());
		steer.limit(e.getPrey().getMaxForce());
		e.getPrey().applyForce(steer);
	}


	private void generateRandomWalkPath() {
		float sum = 0;
		while (sum <= (e.getTimer().getEpisodeTimeFrames() * e.getPrey().getMaxSpeed()) + 15000) {
			PVector previousPosition = randomWalkPositions.get(randomWalkPositions.size()-1);
			PVector newPosition;
			do {
				newPosition = Util.getRandomAdjacentPosition(p, previousPosition, nextPositionRange);
			} while (!Util.withinWindow(p, newPosition));
			randomWalkPositions.add(newPosition);
			sum += PVector.dist(previousPosition, newPosition);
		}
	}

	private void setRandomWalkRange(float range) {
		//convert m to pixels
		nextPositionRange = range * 10;
		generateRandomWalkPath();
	}
}
