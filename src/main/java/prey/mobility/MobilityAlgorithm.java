package prey.mobility;

import prey.Prey;


public interface MobilityAlgorithm {
	public void run();
}
