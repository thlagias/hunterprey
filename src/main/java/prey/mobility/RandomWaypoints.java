package prey.mobility;

import java.util.ArrayList;
import animation.Episode;
import processing.core.PApplet;
import processing.core.PVector;
import util.Util;


public class RandomWaypoints implements MobilityAlgorithm {
	private PApplet p;
	private Episode e;
	private int timeInterval;
	private int timeCount;
	private PVector nextPosition;
	private ArrayList<PVector> randomWaypointsPositions;


	public RandomWaypoints(PApplet parent, Episode episode, int interval) {
		p = parent;
		e = episode;
		//Random Waypoints Motion
		timeCount = 400;
		setRandomWaypointInterval(interval);
		randomWaypointsPositions = new ArrayList<PVector>();
		randomWaypointsPositions.add(e.getPrey().getGlobalPosition().copy());
		// In order to generate directly a new nextPositon
		// within the algorithms we set nextPosition = globalPosition
		nextPosition = e.getPrey().getGlobalPosition().copy();
	}



	public void run() {
		if (timeCount == timeInterval) {
			randomWaypointsPositions.remove(0);
			nextPosition = randomWaypointsPositions.get(0);
			timeCount = 0;
		}
		timeCount++;
		PVector desired = PVector.sub(nextPosition, e.getPrey().getGlobalPosition());
		float d = desired.mag();
		desired.normalize();
		// Arrive
		if (d < 30) {
			float m = p.map(d, 0, 30, 0, e.getPrey().getMaxSpeed());
			desired.mult(m);
		} else {
			desired.mult(e.getPrey().getMaxSpeed());
		}
		PVector steer = PVector.sub(desired, e.getPrey().getGlobalVelocity());
		steer.limit(e.getPrey().getMaxForce());
		e.getPrey().applyForce(steer);
	}


	private void generateRandomWaypointsPath() {
		float sum = 0;
		while (sum <= (e.getTimer().getEpisodeTimeFrames() * e.getPrey().getMaxSpeed()) + 15000) {
			PVector previousPosition = randomWaypointsPositions.get(randomWaypointsPositions.size()-1);
			PVector newPosition = Util.getRandomPosition(p);
			randomWaypointsPositions.add(newPosition);
			sum += PVector.dist(previousPosition, newPosition);
		}
	}

	private void setRandomWaypointInterval(int t) {
		//convert sec to frames
		timeInterval = t * 10;
		generateRandomWaypointsPath();
	}
}
