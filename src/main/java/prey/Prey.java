package prey;

import java.util.ArrayList;
import prey.mobility.MobilityAlgorithm;
import processing.core.PApplet;
import processing.core.PVector;
import util.Util;


public class Prey {
	private PApplet p;
	//global
	private PVector globalPosition;
	private PVector globalVelocity;
	private PVector globalAcceleration;
	private PVector totalForce; 
	private float   maxSpeed;
	private float   maxforce;
	private float   mass;
	//motion
	private MobilityAlgorithm mobilityAlgorithm;
	//display
	private ArrayList<PVector> trajectory;
	private PVector displayPosition;
	private PVector displayVelocity;
	private float   trajectorySize;
	private boolean displayTrajectory;


	public Prey(PApplet parent, MobilityAlgorithm algorithm) {
		p = parent;
		// algorithm
		mobilityAlgorithm = algorithm;
		// global
		//globalPosition = new PVector(700, 600);
		globalPosition = Util.getRandomPosition(p);
		globalVelocity = new PVector(0, 0);
		globalAcceleration = new PVector(0, 0);
		totalForce = new PVector(0, 0);
		maxSpeed = 1;
		maxforce = 10;
		mass = 1;
		// display
		displayPosition = new PVector(globalPosition.x, -globalPosition.y);
		displayVelocity = new PVector(0, 0);
		trajectory      = new ArrayList<PVector>();
	}



	public void run() {
		mobilityAlgorithm.run();
		globalUpdate();
		displayUpdate();
	}


	public void applyForce(PVector force) {
		totalForce.add(force);
	}

	private void globalUpdate() {
		globalAcceleration = totalForce.div(mass);
		globalVelocity.add(globalAcceleration);
		globalVelocity.limit(maxSpeed);
		globalPosition.add(globalVelocity);
		totalForce.mult(0);
	}


	private void displayUpdate() {
		displayPosition.set(globalPosition.x, -globalPosition.y);
		displayVelocity.set(globalVelocity.x, -globalVelocity.y);
		p.ellipseMode(p.CENTER);
		p.stroke(255, 0, 0); 
		p.fill(255, 0, 0);
		p.pushMatrix();
		p.translate(displayPosition.x, displayPosition.y);
		p.rotate(displayVelocity.heading());
		p.stroke(20);
		p.strokeWeight(3);
		p.ellipse(0, 0, 20, 20);
		p.line(0, 0, 10, 0);
		p.strokeWeight(1); //Default
		p.popMatrix();

		//diplay trajectory
		if (displayTrajectory) {
			trajectory.add(displayPosition.copy());
			if (trajectory.size() > trajectorySize) {
				trajectory.remove(0);
			}
			p.beginShape();
			p.stroke(255, 0, 0);
			p.strokeWeight(1);
			p.noFill();
			for (PVector v : trajectory) {
				p.vertex(v.x, v.y);
			}
			p.endShape();
		}
	}




	//////////////////////////////////////////////////////////////////////
	//
	//  SETTERS
	//
	//////////////////////////////////////////////////////////////////////

	public void setMaxSpeed(float v) {
		//no need to convert
		maxSpeed = v;
	}

	public void setMaxForce(float f) {
		maxforce = f;
	}

	public void setMass(float m) {
		mass = m;
	}

	public void setTrajectorySize(float size) {
		trajectorySize = size;
	}

	public void setTrajectoryDisplay(boolean display) {
		displayTrajectory = display;
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  GETTERS
	//
	//////////////////////////////////////////////////////////////////////

	public float getMaxSpeed() {
		//no need to convert
		return maxSpeed;
	}

	public float getMaxForce() {
		return maxforce;
	}

	public float setMass() {
		return mass;
	}

	public PVector getGlobalPosition() {
		return globalPosition;
	}

	public PVector getGlobalVelocity() {
		return globalVelocity;
	}

	public PVector getDisplayPosition() {
		return displayPosition;
	}

	public PVector getDisplayVelocity() {
		return displayVelocity;
	}
}
