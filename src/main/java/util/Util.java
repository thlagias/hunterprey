package util;

import java.util.Random;
import processing.core.PApplet;
import processing.core.PVector;

public class Util {


	//////////////////////////////////////////////////////////////////////
	//  RANDOMIZE
	//////////////////////////////////////////////////////////////////////
	//	public static void initializeRandomSeeds() {
	//		for (int i = 0; i < randomSeedsArray.length; i++) {
	//			randomSeedsArray[i] = i + 1;
	//		}
	//	}

	public static void setRandomSeed(PApplet p, int seed) {
		p.randomSeed(seed);
	}

	public static PVector getRandomPosition(PApplet p) {
		return new PVector(p.random(100, p.width-100), p.random(100, p.height-100));
	}

	public static PVector getRandomAdjacentPosition(PApplet p, PVector oldPosition, float range) {
		float dist = p.random(0, range);
		float theta = p.radians(p.random(0, 360));
		PVector offsetLocation = new PVector(dist*(float)Math.cos(theta), dist*(float)Math.sin(theta));
		PVector newPosition = PVector.add(oldPosition, offsetLocation);
		return newPosition;
	}

	//////////////////////////////////////////////////////////////////////
	//  TRANSFORMATIONS
	//////////////////////////////////////////////////////////////////////
	//	public static PVector localToGlobalCoords(PVector localCoords) {
	//		return null;
	//	}
	//	
	//	public static PVector globalToDisplayCoords(PVector globalCoords) {
	//		return new PVector(globalCoords.x, -globalCoords.y);
	//	}


	//////////////////////////////////////////////////////////////////////
	//  CALCULATIONS
	//////////////////////////////////////////////////////////////////////
	public static float average(float[] values) {
		float sum = 0;
		for (int i = 0; i < values.length; i++) {
			sum += values[i];    
		}
		return sum/values.length;
	}

	public static double normalizeValue(double value, double minValue, double maxValue) {
		return (value - minValue) / (maxValue - minValue);
	}

	public static double denormalizeValue(double normValue, double minValue, double maxValue) {
		return (normValue*(maxValue-minValue)) + minValue;
	}

	public static float log10(float x) {
		return (float)(Math.log(x) / Math.log(10));
	}

	public static int signum(float f) {
		if (f > 0) return 1;
		if (f < 0) return -1;
		return 0;
	} 

	public static float azimuth(PVector point2) {
		return azimuth(new PVector(0, 0), point2);
	}

	public static float azimuth(PVector point1, PVector point2) {
		float azimuth = 0;
		float az1;
		float dx;
		float dy;

		dx = point2.x - point1.x;
		dy = point2.y - point1.y;

		if (dy==0) {

			if (dx==0) {
				azimuth = 0;
				return azimuth;
			} else if (dx>0) {
				azimuth = 90;
				return azimuth;
			} else if (dx<0) {
				azimuth = 270;
				return azimuth;
			}//end if
		} else {

			az1 = 180 * (float)Math.atan(Math.abs(dx/dy))/(float)Math.PI;

			if (dx>0) {   //||||                             
				if (dy>0) {
					azimuth = az1;
					return azimuth;
				} else if (dy==0) {
					azimuth = 90;
					return azimuth;
				} else { //(dy<0)
					azimuth = 180 - az1;
					return azimuth;
				}//end if
			} else if (dx==0) { //||||

				if (dy>0) {
					azimuth = 0;
					return azimuth;
				} else {
					azimuth = 180;
					return azimuth;
				}//end if
			} else if (dx<0) { //||||
				if (dy>0) {
					azimuth = 360 - az1;
					return azimuth;
				} else if (dy<0) {
					azimuth = 180 + az1;
					return azimuth;
				}//end if
			}//end if
		}//end if
		return azimuth;
	}


	public static float bias(float referenceDir, float biasedDir) { //referenceDirection,
		float bias;                                     //biasedDirection

		if (Math.abs(biasedDir - referenceDir) > 180) {
			if (Math.signum(biasedDir - referenceDir) > 0) { 
				bias = -(biasedDir - referenceDir);
			} else if (Math.signum(biasedDir - referenceDir)==0) {
				bias = 0;
			} else {
				bias = biasedDir - referenceDir;
			}//end if

			if (bias<0) {
				bias = bias + 360;
			}//end if
			bias = Math.abs(bias) * (- Math.signum(biasedDir - referenceDir));
		} else if (Math.abs(biasedDir - referenceDir)==0) {
			bias = 0;
		} else {
			bias = biasedDir - referenceDir;
		}//end if

		return bias;
	}//end bias


	public static float signAngleBetween(PVector vector1, PVector vector2) {
		float heading1 = azimuth(vector1);
		float heading2 = azimuth(vector2);
		return bias(heading1, heading2);
	}

	public static boolean withinWindow(PApplet p, PVector location) {
		boolean withinWidth = (0 < location.x && location.x < p.width);
		boolean withinHeight = (0 < location.y && location.y < p.height);
		boolean withinWindow = withinWidth && withinHeight;
		return withinWindow;
	}
}
