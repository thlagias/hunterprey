package util;

public enum Status {
	New, Run, Paused, Finished
}
