package tracker;

import animation.Episode;
import processing.core.PApplet;
import processing.core.PVector;


public class LiveTracker {
	private PApplet p;
	private Episode e;
	private int framecount;
	// mean distance variables
	private float   currentDistance;
	private float   sumDistance;
	private float   meanDistance;         // metric
	// stopped time variables
	private int     hunterIsStoppedFrames;
	private float   stoppedtimeRatio;     // metric
	// traveling efficiency variables
	private PVector hunterPreviousPosition;
	private PVector hunterCurrentPosition;
	private float   hunterTravelledDistance;
	private PVector preyPreviousPosition;
	private PVector preyCurrentPosition;
	private float   preyTravelledDistance;
	private float   travellingEfficiency; // metric
	// score
	private float coefficient;
	private float score;


	public LiveTracker(PApplet parent, Episode episode) {
		p = parent;
		e = episode;
		// mean distance variables
		sumDistance = 0;
		// stopped time variables
		hunterIsStoppedFrames = 0;
		framecount = 0;
		// traveling efficiency variables
		hunterCurrentPosition = e.getHunter().getGlobalPosition().copy();
		preyCurrentPosition   = e.getPrey().getGlobalPosition().copy();
		hunterTravelledDistance = 0;
		preyTravelledDistance = 0;
		// score variables
		coefficient = p.pow(10, 8);
		score = 0;
	}


	public void run() {
		framecount = e.getTimer().getEpisodeFrameCounts();
		updateMeanDistance();         //metric: mean distance between hunter and prey
		updateStoppedTimeRatio();     //metric: hunter stop time / simulation time
		updateTravellingEfficiency(); //metric: prey traveled d / hunter traveled d
		updateScore();                //score
	}



	private void updateMeanDistance() {
		currentDistance = PVector.dist(e.getHunter().getGlobalPosition(), e.getPrey().getGlobalPosition());
		sumDistance += currentDistance;
		meanDistance = sumDistance / framecount; // metric!!
	}

	private void updateStoppedTimeRatio() {
		boolean hunterFreeze      = e.getHunter().getGlobalVelocity().mag() <= 0.0001;
		boolean hunterCloseTarget = currentDistance <= e.getHunter().getArrivalThreshold();
		boolean hunterReachedTarget = hunterFreeze && hunterCloseTarget;
		if (hunterReachedTarget) hunterIsStoppedFrames++;
		stoppedtimeRatio = (float)(hunterIsStoppedFrames+1) / framecount; // metric!!
	}

	private void updateTravellingEfficiency() {
		hunterPreviousPosition = hunterCurrentPosition;
		hunterCurrentPosition = e.getHunter().getGlobalPosition().copy();
		hunterTravelledDistance += PVector.dist(hunterPreviousPosition, hunterCurrentPosition);
		preyPreviousPosition = preyCurrentPosition;
		preyCurrentPosition = e.getPrey().getGlobalPosition().copy();
		preyTravelledDistance += PVector.dist(preyPreviousPosition, preyCurrentPosition);
		travellingEfficiency = preyTravelledDistance / hunterTravelledDistance ; // metric!!
	}

	private void updateScore() {
		score = (coefficient * travellingEfficiency * stoppedtimeRatio) / (p.pow(meanDistance, 2));
	}
}
