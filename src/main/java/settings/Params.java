package settings;

import animation.Episode;
import hunter.Hunter;
import hunter.tracking.Lct;
import hunter.tracking.Lls;
import hunter.tracking.ParticleFilter;
import hunter.tracking.TrackingAlgorithm;
import hunter.tracking.Trilateration;
import prey.Prey;
import prey.mobility.MobilityAlgorithm;
import prey.mobility.RandomWalk;
import processing.core.PApplet;
import processing.data.JSONObject;
import rssi.PathLoss;
import rssi.PathLoss1;
import rssi.PathLoss2;
import rssi.PathLoss3;


public class Params {
	int     rndSeed;
	int     epiTime;
	private JSONObject pathlossParams;
	private JSONObject hunterParams;
	private JSONObject preyParams;
	private JSONObject trackingParams; //only the specified algorithm object
	private JSONObject mobilityParams; //only the specified algorithm object


	public Params(JSONObject defaultPar, JSONObject trackingPar, JSONObject mobilityPar) {
		rndSeed = 1;
		epiTime = defaultPar
				.getJSONObject("simulationParams")
				.getInt("episodeTime");
		pathlossParams  = defaultPar.getJSONObject("pathlossParams");
		hunterParams    = defaultPar.getJSONObject("hunterParams");
		preyParams      = defaultPar.getJSONObject("preyParams");
		String tracking = hunterParams.getString("trackingScheme");
		String mobility = preyParams.getString("mobilityScheme");
		trackingParams  = trackingPar.getJSONObject(tracking);
		mobilityParams  = mobilityPar.getJSONObject(mobility);
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  GETTERS & SETTERS
	//
	//////////////////////////////////////////////////////////////////////
	public void setSeed(int seed) {
		rndSeed = seed;
	}
	
	public int getSeed() {
		return rndSeed;
	}
	
	public int getEpisodeTime() {
		return epiTime;
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  PATH-LOSS SETTERS & GETTERS
	//
	//////////////////////////////////////////////////////////////////////
	protected void setSlowFadingSD(float stdDev) {
		pathlossParams.setFloat("pathlossSD", stdDev);
	}
	
	protected void setPathLossExponent(float pathLossExponent) {
		pathlossParams.setFloat("pathloss_n", pathLossExponent);
	}
	
	protected void setPathLossExpDev(float deviation) {
		pathlossParams.setFloat("pathlossdn", deviation);
	}
	
	protected void setFastFadingM(int m) {
		pathlossParams.setInt("pathlossdn", m);
	}
	
	protected void setFastFadingK(int k) {
		pathlossParams.setInt("pathlossdn", k);
	}
	
	public PathLoss getPathLossModel() {
		PathLoss pathloss = null;
		if (pathlossParams.getString("pathlossModel").equals("PathLoss1")) {
			PathLoss1 pathloss1 = new PathLoss1();
			pathloss1.setFrequency(pathlossParams.getFloat("pathlossFreq"));
			pathloss1.setSlowFadingSD(pathlossParams.getFloat("pathlossSD"));
			pathloss1.setPathLossExponent(pathlossParams.getFloat("pathloss_n"));
			pathloss1.setPathLossExpDev(pathlossParams.getFloat("pathlossdn"));
			pathloss = pathloss1;
		}
		if (pathlossParams.getString("pathlossModel").equals("PathLoss2")) {
			PathLoss2 pathloss2 = new PathLoss2();
			pathloss2.setFrequency(pathlossParams.getFloat("pathlossFreq"));
			pathloss2.setSlowFadingSD(pathlossParams.getFloat("pathlossSD"));
			pathloss2.setFastFadingM(pathlossParams.getInt("pathlossM"));
			pathloss = pathloss2;
		}
		if (pathlossParams.getString("pathlossModel").equals("PathLoss3")) {
			PathLoss3 pathloss3 = new PathLoss3();
			pathloss3.setFrequency(pathlossParams.getFloat("pathlossFreq"));
			pathloss3.setSlowFadingSD(pathlossParams.getFloat("pathlossSD"));
			pathloss3.setFastFadingK(pathlossParams.getInt("pathlossK"));
			pathloss = pathloss3;
		}
		return pathloss;
	}



	//////////////////////////////////////////////////////////////////////
	//
	//  HUNTER SETTERS & GETTERS
	//
	//////////////////////////////////////////////////////////////////////
	public Hunter getHunter(PApplet p, Episode ep) {
		TrackingAlgorithm tracking = null;
		if (hunterParams.getString("trackingScheme").equals("Particle Filters")) {
			float period    = trackingParams.getFloat("period");
			int   particles = trackingParams.getInt("particles");
			float stdDev    = trackingParams.getFloat("sd");
			float preySpd   = trackingParams.getFloat("preySpeed");
			ParticleFilter pf = new ParticleFilter(p, ep, period, particles, stdDev, preySpd);
			tracking = pf;
		}
		if (hunterParams.getString("trackingScheme").equals("LCT")) {
			float period   = trackingParams.getFloat("period");
			float strCoeff = trackingParams.getFloat("cstr");
			Lct lct = new Lct(p, ep, period, strCoeff);
			tracking = lct;
		}
		if (hunterParams.getString("trackingScheme").equals("LLS")) {
			float period = trackingParams.getFloat("period");
			int   window = trackingParams.getInt("window");
			Lls lls = new Lls(p, ep, period, window);
			tracking = lls;
		}
		if (hunterParams.getString("trackingScheme").equals("Trilateration")) {
			boolean sliding = trackingParams.getBoolean("slidingMode");
			Trilateration trl = new Trilateration(p, ep, sliding);
			tracking = trl;
		}
		Hunter hunter = new Hunter(p, tracking);
		hunter.setMaxSpeed(hunterParams.getFloat("hunterMaxSpeed"));
		hunter.setMaxForce(hunterParams.getFloat("hunterMaxForce"));
		hunter.setMass(hunterParams.getFloat("hunterMass"));
		hunter.setArrivalThreshold(hunterParams.getFloat("haltDistance"));
		hunter.setTrajectorySize(hunterParams.getFloat("trajectorySize"));
		hunter.setTrajectoryDisplay(hunterParams.getBoolean("displayTraj"));
		return hunter;
	}



	//////////////////////////////////////////////////////////////////////
	//
	//  PREY SETTERS & GETTERS
	//
	//////////////////////////////////////////////////////////////////////
	protected void setPreyMaxSpeed(float speed) {
		preyParams.setFloat("preyMaxSpeed", speed);
	}
	
	public Prey getPrey(PApplet p, Episode ep) {
		MobilityAlgorithm mobility = null;
		if (preyParams.getString("mobilityScheme").equals("Random Walk")) {
			float range = mobilityParams.getFloat("range");
			RandomWalk randomWalk = new RandomWalk(p, ep, range);
			mobility = randomWalk;
		}
		if (preyParams.getString("mobilityScheme").equals("Random Waypoints")) {
			int interval = mobilityParams.getInt("interval");
			RandomWalk randomWaypoints = new RandomWalk(p, ep, interval);
			mobility = randomWaypoints;
		}
		Prey prey = new Prey(p, mobility);
		prey.setMaxSpeed(preyParams.getFloat("preyMaxSpeed"));
		prey.setMaxForce(preyParams.getFloat("preyMaxForce"));
		prey.setMass(preyParams.getFloat("preyMass"));
		prey.setTrajectorySize(preyParams.getFloat("trajectorySize"));
		prey.setTrajectoryDisplay(preyParams.getBoolean("displayTraject"));
		return prey;
	}
}
