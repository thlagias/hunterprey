package settings;

import java.util.ArrayDeque;
import java.util.Deque;
import processing.core.PApplet;
import processing.data.JSONArray;
import processing.data.JSONObject;


public class Settings {
	private PApplet p;
	private float simulationSpeed;
	private JSONObject defaultParams;
	private JSONObject variableParams;
	private JSONObject trackingParams;
	private JSONObject mobilityParams;
	private Deque<Params> parameters;


	public Settings(PApplet parent) {
		p = parent;
		parameters = new ArrayDeque<Params>();
		// load settings from json files
		defaultParams  = p.loadJSONObject("defaultParams.json");
		variableParams = p.loadJSONObject("variableParams.json");
		trackingParams = p.loadJSONObject("trackingParams.json");
		mobilityParams = p.loadJSONObject("mobilityParams.json");
		
		simulationSpeed = defaultParams
				.getJSONObject("simulationParams")
				.getInt("simulationSpeed");
		
		String model = defaultParams
				.getJSONObject("pathlossParams")
				.getString("pathlossModel");

		JSONArray seedParams = variableParams
				.getJSONObject("simulationParams")
				.getJSONArray("randomSeeds");

		JSONArray sdParams = variableParams
				.getJSONObject("pathlossParams")
				.getJSONArray("pathlossSD");

		JSONArray nParams = variableParams
				.getJSONObject("pathlossParams")
				.getJSONArray("pathloss_n");

		JSONArray dnParams = variableParams
				.getJSONObject("pathlossParams")
				.getJSONArray("pathlossdn");

		JSONArray mParams = variableParams
				.getJSONObject("pathlossParams")
				.getJSONArray("pathlossM");

		JSONArray kParams = variableParams
				.getJSONObject("pathlossParams")
				.getJSONArray("pathlossK");

		JSONArray spdParams = variableParams
				.getJSONObject("preyParams")
				.getJSONArray("preyMaxSpeed");
		
		switch(model) {
		
		case "PathLoss1":
			for(int i = 0; i < sdParams.size(); i++) {
				for(int j = 0; j < seedParams.size(); j++) {
					Params params = new Params(defaultParams, trackingParams, mobilityParams);
					params.setSeed(seedParams.getInt(j));
					params.setSlowFadingSD(sdParams.getFloat(i));
					addParams(params);
				}
			}
			for(int i = 0; i < nParams.size(); i++) {
				for(int j = 0; j < seedParams.size(); j++) {
					Params params = new Params(defaultParams, trackingParams, mobilityParams);
					params.setSeed(seedParams.getInt(j));
					params.setPathLossExponent(nParams.getFloat(i));
					addParams(params);
				}
			}
			for(int i = 0; i < dnParams.size(); i++) {
				for(int j = 0; j < seedParams.size(); j++) {
					Params params = new Params(defaultParams, trackingParams, mobilityParams);
					params.setSeed(seedParams.getInt(j));
					params.setPathLossExpDev(dnParams.getFloat(i));
					addParams(params);
				}
			}
			break;
			
		case "PathLoss2":
			for(int i = 0; i < sdParams.size(); i++) {
				for(int j = 0; j < seedParams.size(); j++) {
					Params params = new Params(defaultParams, trackingParams, mobilityParams);
					params.setSeed(seedParams.getInt(j));
					params.setSlowFadingSD(sdParams.getFloat(i));
					addParams(params);
				}
			}
			for(int i = 0; i < mParams.size(); i++) {
				for(int j = 0; j < seedParams.size(); j++) {
					Params params = new Params(defaultParams, trackingParams, mobilityParams);
					params.setSeed(seedParams.getInt(j));
					params.setFastFadingM(mParams.getInt(i));
					addParams(params);
				}
			}
			break;
			
		case "PathLoss3":
			for(int i = 0; i < sdParams.size(); i++) {
				for(int j = 0; j < seedParams.size(); j++) {
					Params params = new Params(defaultParams, trackingParams, mobilityParams);
					params.setSeed(seedParams.getInt(j));
					params.setSlowFadingSD(sdParams.getFloat(i));
					addParams(params);
				}
			}
			for(int i = 0; i < kParams.size(); i++) {
				for(int j = 0; j < seedParams.size(); j++) {
					Params params = new Params(defaultParams, trackingParams, mobilityParams);
					params.setSeed(seedParams.getInt(j));
					params.setFastFadingK(kParams.getInt(i));
					addParams(params);
				}
			}
			break;
		}
		
		for(int i = 0; i < spdParams.size(); i++) {
			for(int j = 0; j < seedParams.size(); j++) {
				Params params = new Params(defaultParams, trackingParams, mobilityParams);
				params.setSeed(seedParams.getInt(j));
				params.setPreyMaxSpeed(spdParams.getFloat(i));
				addParams(params);
			}
		}
	}
	
	
	public boolean hasNext() {
		if (parameters.peek() != null) return true;
		if (parameters.peek() == null) return false;
		return true;
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  PRIVATE PART
	//
	//////////////////////////////////////////////////////////////////////

	/**
	 * Adds Params element to the tail of deque 
	 * & returns the removed element
	 * @param x  A Params object
	 */
	private void addParams(Params p) {
		parameters.offer(p);
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  GETTERS
	//
	//////////////////////////////////////////////////////////////////////

	/**
	 * Removes Params element from the head of queue 
	 * & returns the removed element
	 * @return Fist Params element from deque
	 */
	public Params getParams() {
		return parameters.poll();
	}

	public float getSimulationSpeed() {
		return simulationSpeed;
	}
}
