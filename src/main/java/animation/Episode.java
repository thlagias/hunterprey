package animation;

import processing.core.PApplet;
import rssi.PathLoss;
import rssi.PathLoss1;
import settings.Params;
import hunter.Hunter;
import hunter.tracking.Lct;
import hunter.tracking.TrackingAlgorithm;
import prey.Prey;
import prey.mobility.MobilityAlgorithm;
import prey.mobility.RandomWalk;
import time.Timer;
import tracker.LiveTracker;
import util.Status;


public class Episode {
	private PApplet  p;
	private Status   status;
	private PathLoss pathloss;
	private Prey     prey;
	private Hunter   hunter;
	private Timer    timer;
	private LiveTracker tracker; //data tracker


	public Episode(PApplet parent, Params params) {
		p = parent;
		p.randomSeed(params.getSeed());
		pathloss = params.getPathLossModel();
		prey     = params.getPrey(p, this);
		hunter   = params.getHunter(p, this);
		tracker  = new LiveTracker(p, this);
		timer    = new Timer(p);
		timer.setEpisodeTime(params.getEpisodeTime());
		timer.stopwatchStartStop();
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  EPISODE STATES & FLOW
	//
	//////////////////////////////////////////////////////////////////////
	protected void commence() {
		status = Status.Run;
	}

	protected void checkTermination() {
		if (timer.finished()) {
			status = Status.Finished;
		}
	}

	protected void terminate() {
		//datatracker.addData(tracker);
	}

	protected void proceedNextEpisode() {
		status = Status.New;
	}

	protected void run() {
		//Timer update
		timer.run();

		//Prey update
		prey.run();

		//Hunter update
		hunter.run();

		//LiveTracker update
		tracker.run();

		//standards
		displayData();
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  GETTERS
	//
	//////////////////////////////////////////////////////////////////////
	public Status getStatus() {
		return status;
	}

	public PathLoss getPathloss() {
		return pathloss;
	}

	public Prey getPrey() {
		return prey;
	}

	public Hunter getHunter() {
		return hunter;
	}

	public Timer getTimer() {
		return timer;
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  PRIVATE PART
	//
	//////////////////////////////////////////////////////////////////////
	private void displayData() {
		p.fill(0, 255, 0);
		p.textSize(11);
		p.text("FPS:      "+  p.frameRate, 10, -665);
		p.text("Hunter x: " + hunter.getGlobalPosition().x, 10, -650);
		p.text("Hunter y: " + hunter.getGlobalPosition().y, 10, -635);
		p.text("Real dist: "+ pathloss.getDistanceReal(), 10, -620);
		p.text("Obs dist: " + pathloss.getDistanceObs(), 10, -605);
		//	  p.text("Tracking: " + hunter.currentAlgorithm, 10, -590);
		//	  p.text("Mobility: " + prey.currentAlgorithm, 10, -575);
		//
		//	  p.text("Parameters", 10, -545);
		//	  p.text("_________________________", 10, -540);
		//	  p.text("RandomSeed:     " + randomSeedsArray[seedIndex-1], 10, -525);
		//	  p.text("StandardDev:      " + pathloss.gaussian.stdDev, 10, -510);
		//	  p.text("Nakagami m:      " + pathloss.nakagami.m, 10, -495);
		//	  p.text("Ricean k:             " + pathloss.ricean.k, 10, -480);
		//	  p.text("PreySpeed:          " + prey.maxSpeed, 10, -465);
		//	  p.text("HunterMass:        " + hunter.mass, 10, -450);
		//	  p.text("LCT Cstr:              " + lctSteeringCoefficient, 10, -435);
		//	  p.text("PF sd:                   " + particlefilterSD, 10, -420);
		//
		//	  p.text("Metrics", 10, -390);
		//	  p.text("_________________________", 10, -385);
		//	  p.text("TotalScore:  " + telemetry.getScore(), 10, -370);
		//	  p.text("MeanDist:   " + telemetry.getMeanDistance()/10, 10, -355);
		//	  p.text("StopTime:   " + telemetry.getStoppedTimeRatio(), 10, -340);
		//	  p.text("TravelEff:    " + telemetry.getTravellingEfficiency(), 10, -325);

		int parameterIndicationY = 0;
		p.stroke(0, 255, 0);
		p.strokeWeight(1.1f);
		p.line(115, parameterIndicationY-3, 125, parameterIndicationY-3);
		p.line(115, parameterIndicationY-3, 118, parameterIndicationY-1);
		p.line(115, parameterIndicationY-3, 118, parameterIndicationY-5);
	}
}
