package animation;

import processing.core.PApplet;
import processing.event.MouseEvent;
import settings.Settings;
import util.Status;



public class Application extends PApplet {
	private Status   appStatus; //application Status
	private Status   epiStatus; //episode Status
	private Settings settings; //loads all parameters
	private Episode  episode;
	private float    simSpeed;
	private float    xo;
	private float    yo;
	private float    pzoom;


	public static void main( String[] args ) {
		PApplet.main("Application");
	}

	public void settings() {
		settings = new Settings(this);
		simSpeed = settings.getSimulationSpeed(); 
		size(1000, 700, FX2D);
		frameRate(10 * simSpeed); // defaultFPS = 10
	}

	public void setup() {
		appStatus = Status.New;
		epiStatus = Status.New;
		//datatracker = new DataTracker();	
	}

	public void draw() {
		//initialize simulation
		if (appStatus == Status.New) {
			background(0);
			//displaySettings();
		}

		//update simulation
		if (appStatus == Status.Run) {
			background(0);
			translate(0, height);
			switch (epiStatus) {
			case New:
				episode = new Episode(this, settings.getParams());
				episode.commence();
				epiStatus = episode.getStatus();
				break;
			case Run:
				episode.run();
				episode.checkTermination();
				epiStatus = episode.getStatus();
				break;
			case Finished:
				episode.terminate();
				episode.proceedNextEpisode();
				epiStatus = episode.getStatus();
				appStatus = checkTermination();
				break;
			}//end switch
			translate(0, -height);
		}

		//end simulation
		if (appStatus == Status.Finished) {
			exit();
		}
	}


	public void keyPressed() {
		if (key == ' ') {
			if (appStatus == Status.New) {
				appStatus = Status.Run;
				return;
			}
			if (appStatus == Status.Run) {
				appStatus = Status.Paused;
				return;
			}
			if (appStatus == Status.Paused) {
				appStatus = Status.Run;
				return;
			}
		}
	}

	public void mouseDragged() {
		xo = xo + (mouseX - pmouseX);
		yo = yo + (mouseY - pmouseY);
	}

	public void mouseWheel(MouseEvent event) {
		float e = event.getCount();
		if (e < 0) pzoom += 0.1;
		if (e > 0) pzoom -= 0.1;
	}


	//////////////////////////////////////////////////////////////////////
	//
	//  PRIVATE PART
	//
	//////////////////////////////////////////////////////////////////////
	private Status checkTermination() {
		if (settings.hasNext()) {
			return Status.Run;
		} else {
			return Status.Finished;
		}
	}
}
