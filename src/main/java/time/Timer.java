package time;

import java.text.NumberFormat;
import processing.core.PApplet;
import java.text.DecimalFormat;


public class Timer {
	private PApplet p; 
	private float dt;
	private boolean countdownEnable = false;
	private boolean stopwatchEnable = false;
	private int minutes;
	private float seconds;
	private float totalSeconds;
	private int episodeTimeFrames;
	private int episodeFrameCounts;
	NumberFormat i2d0;


	public Timer(PApplet parent) {
		this.p = parent;
		this.minutes = 0;
		this.seconds = 0;
		this.totalSeconds = 0;
		this.episodeTimeFrames = 6000; //random big number
		this.episodeFrameCounts = 0;
		i2d0 = new DecimalFormat("00");
	}

	public void countdownStartStop() {
		countdownEnable = !countdownEnable;
	}

	public void stopwatchStartStop() {
		stopwatchEnable = !stopwatchEnable;
	}


	public void run() {
		countdown();
		stopwatch();
		display();
		updateEpisodeFrames();
	}



	private void countdown() {
		if (countdownEnable) {
			dt = (float)1/10;
			totalSeconds = totalSeconds - dt;
			this.updateTime(totalSeconds);
		}
	}

	private void stopwatch() {
		if (stopwatchEnable) {
			dt = (float)1/10;
			totalSeconds = totalSeconds + dt;
			this.updateTime(totalSeconds);
		}
	} 

	private void updateTime(float totalSecs) {
		this.minutes = (int)(totalSecs/60);
		this.seconds = totalSecs % 60;
	}

	private void display() {
		p.fill(0, 255, 0);
		p.textSize(11);
		p.text("Timer:   " +i2d0.format(this.getMinutes())+":"+
				i2d0.format(((int)this.getSeconds())), 
				10, -680);
	}

	private void updateEpisodeFrames() {
		episodeFrameCounts++;
	}

	//////////////////////////////////////////////////////////////////////
	//
	//  setters
	//
	//////////////////////////////////////////////////////////////////////
	public void setEpisodeTime(int t) { //in seconds
		this.episodeTimeFrames = t * 10;
	}

	//////////////////////////////////////////////////////////////////////
	//
	//  getters
	//
	//////////////////////////////////////////////////////////////////////
	public int getMinutes() {
		return this.minutes;
	}

	public float getSeconds() {
		return this.seconds;
	}

	public float getTotalSecs() {
		return this.totalSeconds;
	}

	public int getEpisodeFrameCounts() {
		return this.episodeFrameCounts;
	}

	public int getEpisodeTimeFrames() {
		return this.episodeTimeFrames;
	}

	public boolean finished() {
		boolean episodeFinished = false;
		if (episodeFrameCounts < episodeTimeFrames) episodeFinished = false;
		if (episodeFrameCounts == episodeTimeFrames) episodeFinished = true;
		return episodeFinished;
	}
}
