### Hunter-Prey Simulation

A Hunter-Prey scenario simulation using Received Signal Strength(RSS) proximity measurements.
A number of different scenarios can be tested and evaluated using a variety of:
* Hunter's Tracking Algorithms
* Prey's Mobility Algorithms
* Path-Loss models

Tracking Algorithms used by Hunter:
* [Trilateration](https://en.wikipedia.org/wiki/Trilateration)
* Law of Cosines Tracking (LCT)
* [Linear Least Squares (LLS)](https://en.wikipedia.org/wiki/Linear_least_squares_(mathematics))
* [Particle Filter (PF)](https://en.wikipedia.org/wiki/Particle_filter)

Mobility Algorithms used by Prey:
* [Random Walk](https://en.wikipedia.org/wiki/Random_walk)
* [Random Waypoint](https://en.wikipedia.org/wiki/Random_waypoint_model)

Path-Loss Models:
1. slow fading errors: Gaussian, fast fading errors: -
2. slow fading errors: Gaussian, fast fading errors: Nakagami
3. slow fading errors: Gaussian, fast fading errors: Ricean

------------------------
### Useful links
* [Path Loss](https://en.wikipedia.org/wiki/Path_loss)
* [Free-space Path Loss](https://en.wikipedia.org/wiki/Free-space_path_loss)
* [Log-distance Path Loss](https://en.wikipedia.org/wiki/Log-distance_path_loss_model)

* [Gaussian distribution](https://en.wikipedia.org/wiki/Normal_distribution)
* [Nakagami distribution](https://en.wikipedia.org/wiki/Nakagami_distribution)
* [Ricean distribution](https://en.wikipedia.org/wiki/Rice_distribution)

------------------------
### Related Publications
* [New RSSI-based Tracking for Following Mobile Targets using the Law of Cosines](https://ieeexplore.ieee.org/document/8128482/)

------------------------
### Built With
* Java
* [Maven](https://maven.apache.org/)
* [Processing](https://processing.org/)

------------------------

